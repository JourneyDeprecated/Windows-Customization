~created by: voxeledphoton~
-> http://voxeledphoton.deviantart.com/

I created these for my girlfriend and I <3
but anyway if ur getting this just cause I hope you enjoy
you too love ^^

I may include updates in the future such as new colors and such if I get enough requests...
░░░░░░░▒_▒_▒  ░░░░░░░
░░░░░░_▒▒▒▒▒_▒░░░░░░
░░▓  ░░▒▒▒▒▒▒▒▒░░  ▓░░
░░__░░_▒▒▒▒▒▒_░░__░░           lol
░░░░░░▒▒▒▒▒▒▒▒░░░░░░
░░░░░░_▒▒▒▒▒▒_░░░░░░
░░░░░░▒▒▒▒▒▒▒▒░░░░░░
░░░░░░░▒_▒_▒_░░░░░░░

// INSTALL //
if your wondering how to install these awesome cursors just find your control panel (usually found by right clicking desktop and click personalize or something)
and go to where it says change mouse pointers [I know this is how it works with windows 8] idk just google it or something ;b 

License
-----------
Creative Commons License.
Some rights reserved. This work is licensed under a Creative Commons Attribution-Noncommercial-Share Alike 3.0 License.

You are free:
to Share — to copy, distribute and transmit the work 
to Remix — to adapt the work 

Under the following conditions:
Attribution — You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).  

Noncommercial — You may not use this work for commercial purposes. 

Share Alike — If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one. 

With the understanding that: 
Waiver — Any of the above conditions can be waived if you get permission from the copyright holder. 
Public Domain — Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license. 
Other Rights — In no way are any of the following rights affected by the license: • Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations; 
• The author's moral rights; 
• Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights. 

Notice — For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page. 